# README #

This version of Bootstrap has been modified to give this framework a valid CSS.
This choice was made to make it "compatible" with certain contracts, laws, and regulations which provide that the code should be written (as specific) a valid CSS.

### It's fully compatible? ###

Unfortunately no:

* Nothing animations
* Compatibility to older browsers not guaranteed (it works, but it is possible that some graphic details are shown in a different way)
* Some components have been removed (animated bars)

### How do I get set up? ###

* Import the original version of bootstrap
* Add to the css file "bootstrap-w3c.css"
* Include the new HTML file instead of the original
* Also check that you've produced a Valid W3C HTML

### Contribution guidelines ###

If you want to contribute or help me, feel free to do so by writing an email to: paolo @ 2dweb.it